package com.prototype.userapi.infrastructure.assembler.usecase.service;

import static com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.mapper.PayUMapper.fromResponse;
import static com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.mapper.PayUMapper.toTokenizeCard;

import com.prototype.userapi.domain.kernel.command.user.TokenizeCardCommand;
import com.prototype.userapi.domain.kernel.exception.TransactionException;
import com.prototype.userapi.domain.kernel.model.ResultTransaction;
import com.prototype.userapi.domain.usecase.service.TransactionService;
import com.prototype.userapi.infrastructure.adapter.outbound.api.payu.PayUClient;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PayUTransactionService implements TransactionService {

	private static final String MESSAGE = "Getting error tokenizing card for user %s";
	private final PayUClient tokenizeCardCall;

	@Override public ResultTransaction tokenizeCard(final TokenizeCardCommand command) {

		log.info(String.format("Starting tokenize card [%s] for user [%s]", command.getNumber().substring(12), command.getUserId()));
		return Try.of(() -> toTokenizeCard(command))
				  .peek(payURequest -> String
						  .format("Prepare to send card [%s] to PayU", payURequest.getCreditCardToken().getNumber().substring(12)))
				  .transform(payURequests -> Try.of(() -> tokenizeCardCall.call(payURequests.get())))
				  .peek(payUResponse -> String.format("Getting response from payU response with status [%s]", payUResponse.getCode()))
				  .filter(payUResponse -> "SUCCESS".equals(payUResponse.getCode()))
				  .map(payUResponse -> fromResponse(payUResponse.getCreditCardToken()))
				  .onFailure(throwable -> log.error(String.format(MESSAGE, command.getUserId()), throwable))
				  .getOrElseThrow(throwable -> new TransactionException(throwable.getMessage()));
	}
}
