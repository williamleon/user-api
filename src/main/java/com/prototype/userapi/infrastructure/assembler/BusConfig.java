package com.prototype.userapi.infrastructure.assembler;

import com.prototype.userapi.domain.kernel.command.CommandExecute;
import com.prototype.userapi.domain.kernel.command.authentication.ImmutableCompleteRegistry;
import com.prototype.userapi.domain.kernel.command.authentication.ImmutableFastRegistry;
import com.prototype.userapi.domain.kernel.command.authentication.ImmutableLogin;
import com.prototype.userapi.domain.kernel.command.authentication.ImmutableRegistry;
import com.prototype.userapi.domain.kernel.command.user.ImmutableTokenizeCardCommand;
import com.prototype.userapi.domain.kernel.event.EventHandler;
import com.prototype.userapi.domain.kernel.event.user.ImmutableUserEvent;
import com.prototype.userapi.domain.kernel.query.QueryHandler;
import com.prototype.userapi.domain.kernel.query.user.ImmutableFindCardsQuery;
import com.prototype.userapi.domain.kernel.query.user.ImmutableUserInformationQuery;
import com.prototype.userapi.domain.usecase.LocalCommandBus;
import com.prototype.userapi.domain.usecase.LocalEventBus;
import com.prototype.userapi.domain.usecase.LocalQueryBus;
import io.vavr.collection.HashMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure Query/Command bus
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Configuration
public class BusConfig {

	@Bean
	public LocalCommandBus commandBus(CommandExecute registryCommandExecute,
									  CommandExecute fastRegistryCommandExecute,
									  CommandExecute completeRegistryCommandExecute,
									  CommandExecute loginCommandExecuteImp,
									  CommandExecute tokenizeCardCommandExecute) {

		return new LocalCommandBus(HashMap.of(ImmutableRegistry.class.getName(), registryCommandExecute,
											  ImmutableFastRegistry.class.getName(), fastRegistryCommandExecute,
											  ImmutableCompleteRegistry.class.getName(), completeRegistryCommandExecute,
											  ImmutableLogin.class.getName(), loginCommandExecuteImp,
											  ImmutableTokenizeCardCommand.class.getName(), tokenizeCardCommandExecute));
	}

	@Bean
	public LocalEventBus eventBus(EventHandler userEventHandler) {

		return new LocalEventBus(HashMap.of(ImmutableUserEvent.class.getName(), userEventHandler));
	}

	@Bean
	public LocalQueryBus queryBus(QueryHandler userInformationQueryHandler,
								  QueryHandler findCardsQueryHandler) {

		return new LocalQueryBus(HashMap.of(ImmutableUserInformationQuery.class.getName(), userInformationQueryHandler,
											ImmutableFindCardsQuery.class.getName(), findCardsQueryHandler));
	}
}
