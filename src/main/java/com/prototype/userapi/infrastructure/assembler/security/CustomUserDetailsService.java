package com.prototype.userapi.infrastructure.assembler.security;

import com.prototype.userapi.domain.kernel.exception.ResourceNotFoundException;
import com.prototype.userapi.infrastructure.adapter.outbound.database.repository.DBUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    final DBUserRepository DBUserRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        return DBUserRepository.findByEmail(email)
                               .map(UserPrincipal::create)
                               .orElseThrow(() -> new UsernameNotFoundException("User not found with email : " + email));
    }

    @Transactional
    public UserDetails loadUserById(String id) {

        return DBUserRepository.findById(id)
                               .map(UserPrincipal::create)
                               .orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
    }
}
