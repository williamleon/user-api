package com.prototype.userapi.infrastructure.assembler.security;

import io.vavr.control.Option;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class TokenAuthenticationFilter extends OncePerRequestFilter {

	private static final String BEARER_STRING = "Bearer ";
	private static final String AUTHORIZATION_HEADER = "Authorization";

	@Autowired
	private TokenProvider tokenProvider;

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Override
	protected void doFilterInternal(HttpServletRequest request,
									HttpServletResponse response,
									FilterChain filterChain) throws ServletException, IOException {

		Try.of(() -> getJwtFromRequest(request)
				.filter(StringUtils::hasText)
				.filter(tokenProvider::validateToken)
				.map(tokenProvider::getUserIdFromToken)
				.map(customUserDetailsService::loadUserById)
				.map(userDetails -> getAuthenticationToken(request, userDetails))
				.get())
		   .peek(authentication -> SecurityContextHolder.getContext().setAuthentication(authentication))
		   .onFailure(throwable -> log.error("Could not set user authentication in security context", throwable));

		filterChain.doFilter(request, response);
	}

	private UsernamePasswordAuthenticationToken getAuthenticationToken(HttpServletRequest request, UserDetails userDetails) {

		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
				userDetails, null,
				userDetails.getAuthorities());
		authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
		return authentication;
	}

	private Option<String> getJwtFromRequest(HttpServletRequest request) {

		return Option.of(request)
					 .map(r -> r.getHeader(AUTHORIZATION_HEADER))
					 .filter(StringUtils::hasText)
					 .filter(header -> header.startsWith(BEARER_STRING))
					 .map(header -> header.substring(7));
	}
}
