package com.prototype.userapi.infrastructure.assembler;

import com.prototype.userapi.domain.usecase.util.EncoderUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * EncoderUtil implementation
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@RequiredArgsConstructor
@Component
public class SecureEncoderUtil implements EncoderUtil {

	private final PasswordEncoder encoder;

	/**
	 * Use BCrip to encode given value
	 *
	 * @param value
	 * @return
	 */
	@Override public String encode(final String value) {

		return encoder.encode(value);
	}
}
