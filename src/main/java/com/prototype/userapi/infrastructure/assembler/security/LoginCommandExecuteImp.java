package com.prototype.userapi.infrastructure.assembler.security;

import com.prototype.userapi.domain.kernel.command.authentication.Login;
import com.prototype.userapi.domain.kernel.model.ImmutableTokenId;
import com.prototype.userapi.domain.kernel.model.TokenId;
import com.prototype.userapi.domain.usecase.service.ILoginCommandExecute;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */

@Slf4j
@Service
@AllArgsConstructor
public class LoginCommandExecuteImp implements ILoginCommandExecute {

	private final AuthenticationManager authenticationManager;
	private final TokenProvider tokenProvider;

	/**
	 * @param command
	 * @return
	 */
	@Override public Either<Throwable, TokenId> execute(final Login command) {

		return Try.of(() -> authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(command.getEmail(),
														command.getPassword())))
				  .peek(authentication -> SecurityContextHolder.getContext().setAuthentication(authentication))
				  .map(authentication -> tokenProvider.createToken(authentication))
				  .map(s -> (TokenId) ImmutableTokenId.builder().token(s).build())
				  .onFailure(throwable -> log.error("Authentication error", throwable))
				  .toEither();
	}
}
