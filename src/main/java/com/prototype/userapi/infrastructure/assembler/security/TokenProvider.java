package com.prototype.userapi.infrastructure.assembler.security;

import com.prototype.userapi.infrastructure.assembler.AppProperties;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class TokenProvider {

	private final AppProperties appProperties;

	public String createToken(Authentication authentication) {

		UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
		log.info(String.valueOf(userPrincipal.getAuthorities().size()));

		final String authorities = authentication.getAuthorities().stream()
												 .map(GrantedAuthority::getAuthority)
												 .collect(Collectors.joining(","));

		Date now = new Date();
		Date expiryDate = new Date(now.getTime() + appProperties.getAuth().getTokenExpirationMsec());

		return Jwts.builder()
				   .setSubject(userPrincipal.getId())
				   .setIssuedAt(new Date())
				   .setExpiration(expiryDate)
				   .claim("authorities", authorities)
				   .signWith(SignatureAlgorithm.HS512, appProperties.getAuth().getTokenSecret())
				   .compact();
	}

	public String getUserIdFromToken(String token) {

		return Jwts.parser()
				   .setSigningKey(appProperties.getAuth().getTokenSecret())
				   .parseClaimsJws(token)
				   .getBody().getSubject();

	}

	public boolean validateToken(String authToken) {

		try {
			Jwts.parser().setSigningKey(appProperties.getAuth().getTokenSecret()).parseClaimsJws(authToken);
			return true;
		} catch (SignatureException ex) {
			log.error("Invalid JWT signature");
		} catch (MalformedJwtException ex) {
			log.error("Invalid JWT token");
		} catch (ExpiredJwtException ex) {
			log.error("Expired JWT token");
		} catch (UnsupportedJwtException ex) {
			log.error("Unsupported JWT token");
		} catch (IllegalArgumentException ex) {
			log.error("JWT claims string is empty.");
		}
		return false;
	}
}
