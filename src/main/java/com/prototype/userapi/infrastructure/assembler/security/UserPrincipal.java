package com.prototype.userapi.infrastructure.assembler.security;

import com.prototype.userapi.infrastructure.adapter.outbound.database.model.DBUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class UserPrincipal implements OAuth2User, UserDetails {

	private String id;
	private String email;
	private String password;
	private Collection<? extends GrantedAuthority> authorities;

	public UserPrincipal(String id, String email, String password, Collection<? extends GrantedAuthority> authorities) {

		this.id = id;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
	}

	public static UserPrincipal create(DBUser DBUser) {

		List<SimpleGrantedAuthority> authorityList;
		if (DBUser.getIsAdmin()) {
			authorityList = List.of(new SimpleGrantedAuthority("ROLE_USER"),
									new SimpleGrantedAuthority("ROLE_ADMIN"));
		} else {
			authorityList = List.of(new SimpleGrantedAuthority("ROLE_USER"));
		}
		return new UserPrincipal(
				DBUser.getId(),
				DBUser.getEmail(),
				DBUser.getPassword(),
				authorityList);
	}

	public String getId() {

		return id;
	}

	public String getEmail() {

		return email;
	}

	@Override
	public String getPassword() {

		return password;
	}

	@Override
	public String getUsername() {

		return email;
	}

	@Override
	public boolean isAccountNonExpired() {

		return true;
	}

	@Override
	public boolean isAccountNonLocked() {

		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {

		return true;
	}

	@Override
	public boolean isEnabled() {

		return true;
	}

	@Override public Map<String, Object> getAttributes() {

		return null;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		return authorities;
	}

	@Override
	public String getName() {

		return id;
	}
}
