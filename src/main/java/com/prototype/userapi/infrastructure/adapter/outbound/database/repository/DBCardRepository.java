package com.prototype.userapi.infrastructure.adapter.outbound.database.repository;

import java.util.List;

import com.prototype.userapi.infrastructure.adapter.outbound.database.model.DBCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Repository
public interface DBCardRepository extends JpaRepository<DBCard, String> {

	List<DBCard> findAllByUserId(String userId);

}
