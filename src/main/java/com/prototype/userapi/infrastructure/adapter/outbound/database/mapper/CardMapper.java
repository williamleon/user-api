package com.prototype.userapi.infrastructure.adapter.outbound.database.mapper;

import com.prototype.userapi.domain.kernel.model.Card;
import com.prototype.userapi.domain.kernel.model.ImmutableCard;
import com.prototype.userapi.infrastructure.adapter.outbound.database.model.DBCard;
import lombok.NoArgsConstructor;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@NoArgsConstructor
public class CardMapper {

	public static Card fromDBCard(DBCard card) {

		return ImmutableCard.builder()
							.creditCardTokenId(card.getCreditCardTokenId())
							.name(card.getName())
							.paymentMethod(card.getPaymentMethod())
							.maskedNumber(card.getMaskedNumber())
							.userId(card.getUserId())
							.build();
	}

	public static DBCard fromCard(Card card) {

		return DBCard.builder()
					 .creditCardTokenId(card.getCreditCardTokenId())
					 .name(card.getName())
					 .paymentMethod(card.getPaymentMethod())
					 .maskedNumber(card.getMaskedNumber())
					 .userId(card.getUserId())
					 .build();
	}

}
