package com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.mapper;

import com.prototype.userapi.domain.kernel.command.user.TokenizeCardCommand;
import com.prototype.userapi.domain.kernel.model.ImmutableResultTransaction;
import com.prototype.userapi.domain.kernel.model.ResultTransaction;
import com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.CreditCardToken;
import com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.MerchantCredentials;
import com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.PayURequest;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class PayUMapper {

	private static final String API_KEY = "4Vj8eK4rloUd272L48hsrarnUA";
	private static final String API_LOGIN = "pRRXKOl8ikMmt9u";

	public static ResultTransaction fromResponse(CreditCardToken response) {

		return ImmutableResultTransaction.builder()
										 .creditCardTokenId(response.getCreditCardTokenId())
										 .name(response.getName())
										 .identificationNumber(response.getIdentificationNumber())
										 .paymentMethod(response.getPaymentMethod())
										 .maskedNumber(response.getMaskedNumber())
										 .payerId(response.getPayerId())
										 .build();
	}

	public static PayURequest toTokenizeCard(final TokenizeCardCommand command) {

		return PayURequest.builder()
						  .merchant(getMerchantCredentials())
						  .creditCardToken(CreditCardToken.builder()
														  .payerId(command.getUserId())
														  .name(command.getName())
														  .identificationNumber(command.getIdentificationNumber())
														  .paymentMethod(command.getPaymentMethod())
														  .number(command.getNumber())
														  .expirationDate(command.getExpirationDate())
														  .build())
						  .build();
	}

	private static MerchantCredentials getMerchantCredentials() {

		return MerchantCredentials.builder()
								  .apiKey(API_KEY)
								  .apiLogin(API_LOGIN)
								  .build();
	}

}
