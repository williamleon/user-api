package com.prototype.userapi.infrastructure.adapter.outbound.api.payu;

import com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.PayURequest;
import com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.PayUResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface PayUApi {

	@Headers({
			"Accept: application/json",
			"Content-Type: application/json"
	})
	@POST("payments-api/4.0/service.cgi")
	Call<PayUResponse> tokenizeCard(@Body PayURequest request);

}
