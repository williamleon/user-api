package com.prototype.userapi.infrastructure.adapter.outbound.database.repository;

import static com.prototype.userapi.infrastructure.adapter.outbound.database.mapper.CardMapper.fromCard;
import static com.prototype.userapi.infrastructure.adapter.outbound.database.mapper.CardMapper.fromDBCard;

import java.util.List;
import java.util.stream.Collectors;

import com.prototype.userapi.domain.kernel.model.Card;
import com.prototype.userapi.domain.usecase.repository.CardRepository;
import com.prototype.userapi.infrastructure.adapter.outbound.database.mapper.CardMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@RequiredArgsConstructor
@Component
public class MapperCardRepository implements CardRepository {

	private final DBCardRepository repository;

	@Override public List<Card> findAllByUserId(final String userId) {

		return repository.findAllByUserId(userId).stream()
						 .map(CardMapper::fromDBCard)
						 .collect(Collectors.toList());
	}

	@Override public Card save(final Card card) {

		return fromDBCard(repository.save(fromCard(card)));
	}
}
