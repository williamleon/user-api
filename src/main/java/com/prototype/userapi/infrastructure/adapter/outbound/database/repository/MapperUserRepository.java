package com.prototype.userapi.infrastructure.adapter.outbound.database.repository;

import static com.prototype.userapi.infrastructure.adapter.outbound.database.mapper.UserMapper.formDBUser;
import static com.prototype.userapi.infrastructure.adapter.outbound.database.mapper.UserMapper.fromUser;

import java.util.Optional;

import com.prototype.userapi.domain.kernel.model.User;
import com.prototype.userapi.domain.usecase.repository.UserRepository;
import com.prototype.userapi.infrastructure.adapter.outbound.database.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Implementation of {@link UserRepository}
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@RequiredArgsConstructor
@Component
public class MapperUserRepository implements UserRepository {

	private final DBUserRepository dbUserRepository;

	@Override public Optional<User> findUserByEmail(final String email) {

		return dbUserRepository
				.findByEmail(email)
				.map(UserMapper::formDBUser);
	}

	@Override public User save(final User user) {

		return formDBUser(dbUserRepository.save(fromUser(user)));
	}

	@Override public Optional<User> findById(final String id) {

		return dbUserRepository.findById(id).map(UserMapper::formDBUser);
	}
}
