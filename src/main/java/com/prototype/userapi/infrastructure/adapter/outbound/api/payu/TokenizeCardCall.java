package com.prototype.userapi.infrastructure.adapter.outbound.api.payu;

import java.io.IOException;

import com.prototype.userapi.infrastructure.adapter.outbound.api.ApiClientUtil;
import com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.PayURequest;
import com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model.PayUResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class TokenizeCardCall implements PayUClient {

	@Value("${api.payu.url}")
	private String baseUrl;

	private final ApiClientUtil util;

	@Override public PayUResponse call(final PayURequest request) throws IOException {

		Retrofit retrofit = util.getClient(baseUrl);
		PayUApi api = retrofit.create(PayUApi.class);
		return api.tokenizeCard(request).execute().body();
	}

	@Override public void onResponse(final Call<PayUResponse> call, final Response<PayUResponse> response) {

	}

	@Override public void onFailure(final Call<PayUResponse> call, final Throwable throwable) {

		log.error("Error calling tokenize card in PayU Api", throwable);
	}
}
