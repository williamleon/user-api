package com.prototype.userapi.infrastructure.adapter.outbound.database.mapper;

import java.util.Optional;

import com.prototype.userapi.domain.kernel.model.ImmutableUser;
import com.prototype.userapi.domain.kernel.model.User;
import com.prototype.userapi.infrastructure.adapter.outbound.database.model.DBUser;
import lombok.NoArgsConstructor;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@NoArgsConstructor
public class UserMapper {

	public static final User formDBUser(DBUser dbUser){
		return ImmutableUser.builder()
							.id(dbUser.getId())
							.name(Optional.ofNullable(dbUser.getName()))
							.lastname(Optional.ofNullable(dbUser.getLastname()))
							.email(dbUser.getEmail())
							.identification(Optional.ofNullable(dbUser.getIdentification()))
							.phoneNumber(Optional.ofNullable(dbUser.getPhoneNumber()))
							.gender(Optional.ofNullable(dbUser.getGender()))
							.admin(dbUser.getIsAdmin())
							.password(Optional.ofNullable(dbUser.getPassword()))
							.build();
	}

	public static final DBUser fromUser(User user){
		return DBUser.builder()
					 .id(user.getId().orElse(null))
					 .name(user.getName().orElse(null))
					 .lastname(user.getLastname().orElse(null))
					 .email(user.getEmail())
					 .identification(user.getIdentification().orElse(null))
					 .phoneNumber(user.getPhoneNumber().orElse(null))
					 .gender(user.getGender().orElse(null))
					 .isAdmin(user.isAdmin())
					 .password(user.getPassword().orElse(null))
					 .build();
	}
}
