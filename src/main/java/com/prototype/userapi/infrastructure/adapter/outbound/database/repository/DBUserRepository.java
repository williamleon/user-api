package com.prototype.userapi.infrastructure.adapter.outbound.database.repository;

import java.util.Optional;

import com.prototype.userapi.infrastructure.adapter.outbound.database.model.DBUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DBUserRepository extends JpaRepository<DBUser, String> {

	Optional<DBUser> findByEmail(String email);

	Boolean existsByEmail(String email);

}
