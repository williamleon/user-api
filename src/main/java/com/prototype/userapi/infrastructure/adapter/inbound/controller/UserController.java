package com.prototype.userapi.infrastructure.adapter.inbound.controller;

import static com.prototype.userapi.infrastructure.adapter.inbound.controller.ResponseUtil.getMessage;

import java.util.List;
import javax.validation.Valid;

import com.prototype.userapi.domain.kernel.command.CommandBus;
import com.prototype.userapi.domain.kernel.command.user.TokenizeCardCommand;
import com.prototype.userapi.domain.kernel.model.Card;
import com.prototype.userapi.domain.kernel.model.User;
import com.prototype.userapi.domain.kernel.query.QueryBus;
import com.prototype.userapi.domain.kernel.query.user.ImmutableFindCardsQuery;
import com.prototype.userapi.domain.kernel.query.user.ImmutableUserInformationQuery;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

	private final CommandBus commandBus;
	private final QueryBus queryBus;

	@GetMapping
	public ResponseEntity<?> findUser() {

		String userId = SecurityContextHolder.getContext().getAuthentication().getName();
		Either<Throwable, User> handle = queryBus.ask(ImmutableUserInformationQuery.builder().userId(userId).build());
		return handle.map(ResponseEntity::ok)
					 .getOrElseGet(throwable -> new ResponseEntity(getMessage(throwable.getMessage()), HttpStatus.CONFLICT));
	}

	@PostMapping("/card/tokenize")
	public ResponseEntity<?> tokenizeCard(@Valid @RequestBody TokenizeCardCommand command) {

		Either<Throwable, Card> execution = commandBus.dispatch(command);
		return execution.map(card -> ResponseEntity.noContent().build())
						.getOrElseGet(throwable -> new ResponseEntity(getMessage(throwable.getMessage()), HttpStatus.CONFLICT));
	}

	@GetMapping("/card")
	public ResponseEntity<?> findCards() {

		String userId = SecurityContextHolder.getContext().getAuthentication().getName();
		Either<Throwable, List<Card>> handle = queryBus.ask(ImmutableFindCardsQuery.builder().userId(userId).build());
		return handle.map(ResponseEntity::ok)
					 .getOrElseGet(throwable -> new ResponseEntity(getMessage(throwable.getMessage()), HttpStatus.CONFLICT));
	}

}
