package com.prototype.userapi.infrastructure.adapter.inbound.controller;

import static com.prototype.userapi.infrastructure.adapter.inbound.controller.ResponseUtil.getMessage;

import javax.validation.Valid;

import com.prototype.userapi.domain.kernel.command.CommandBus;
import com.prototype.userapi.domain.kernel.command.authentication.CompleteRegistry;
import com.prototype.userapi.domain.kernel.command.authentication.FastRegistry;
import com.prototype.userapi.domain.kernel.command.authentication.Login;
import com.prototype.userapi.domain.kernel.command.authentication.Registry;
import com.prototype.userapi.domain.kernel.model.TokenId;
import com.prototype.userapi.domain.kernel.model.User;
import com.prototype.userapi.domain.kernel.model.UserId;
import io.vavr.control.Either;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/auth")
public class AuthController {

	private final CommandBus bus;

	@PostMapping("/registry")
	public ResponseEntity<?> register(@Valid @RequestBody Registry registryRequest) {

		Either<Throwable, User> execution = bus.dispatch(registryRequest);
		return execution.map(user -> ResponseEntity.noContent().build())
						.getOrElseGet(throwable -> new ResponseEntity<>(getMessage(throwable.getMessage()), HttpStatus.CONFLICT));
	}

	@PostMapping("/fast-registry")
	public ResponseEntity<?> fastRegister(@Valid @RequestBody FastRegistry fastRegistry) {

		Either<Throwable, UserId> execution = bus.dispatch(fastRegistry);
		return execution.map(ResponseEntity::ok)
						.getOrElseGet(throwable -> new ResponseEntity(getMessage(throwable.getMessage()), HttpStatus.BAD_REQUEST));
	}

	@PostMapping("/complete-registry")
	public ResponseEntity<?> completeRegister(@Valid @RequestBody CompleteRegistry completeRegistry) {

		Either<Throwable, User> execution = bus.dispatch(completeRegistry);
		return execution.map(user -> ResponseEntity.ok().build())
						.getOrElseGet(throwable -> new ResponseEntity<>(getMessage(throwable.getMessage()), HttpStatus.CONFLICT));

	}

	@PostMapping("/login")
	public ResponseEntity<?> login(@Valid @RequestBody Login login) {

		Either<Throwable, TokenId> execution = bus.dispatch(login);
		return execution.map(tokenId -> ResponseEntity.ok(tokenId))
						.getOrElseGet(throwable -> new ResponseEntity(getMessage(throwable.getMessage()), HttpStatus.UNAUTHORIZED));
	}

}
