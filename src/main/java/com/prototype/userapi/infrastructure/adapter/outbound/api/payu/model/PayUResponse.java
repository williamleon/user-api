package com.prototype.userapi.infrastructure.adapter.outbound.api.payu.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PayUResponse {

	private String code;
	private String error;
	private CreditCardToken creditCardToken;
}
