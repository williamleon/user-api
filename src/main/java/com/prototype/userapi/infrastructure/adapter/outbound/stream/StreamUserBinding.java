package com.prototype.userapi.infrastructure.adapter.outbound.stream;

import com.prototype.userapi.domain.kernel.event.user.UserEvent;
import com.prototype.userapi.domain.usecase.stream.UserBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class StreamUserBinding implements UserBinding {

	private final OutputBinding outputBinding;

	@Override public void send(final UserEvent user) {
		log.info(String.format("Sending event for user %s", user.getUser().getId()));
		outputBinding.userChannel()
					 .send(MessageBuilder
								   .withPayload(user)
								   .build());
	}
}
