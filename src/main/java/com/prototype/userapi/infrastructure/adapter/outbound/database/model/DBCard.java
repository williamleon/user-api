package com.prototype.userapi.infrastructure.adapter.outbound.database.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */

@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cards")
public class DBCard {

	@Id
	private String creditCardTokenId;
	private String name;
	private String paymentMethod;
	private String maskedNumber;
	private String userId;
}
