package com.prototype.userapi;

import com.prototype.userapi.infrastructure.adapter.outbound.stream.OutputBinding;
import com.prototype.userapi.infrastructure.assembler.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(OutputBinding.class)
@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class UserApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserApiApplication.class, args);
	}

}
