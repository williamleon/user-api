package com.prototype.userapi.domain.kernel.query.user;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.userapi.domain.kernel.query.Query;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableUserInformationQuery.class)
@JsonSerialize(as = ImmutableUserInformationQuery.class)
public interface UserInformationQuery extends Query {

	String getUserId();
}
