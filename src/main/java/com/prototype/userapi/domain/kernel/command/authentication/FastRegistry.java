package com.prototype.userapi.domain.kernel.command.authentication;

import javax.validation.constraints.Email;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.userapi.domain.kernel.command.Command;
import org.immutables.value.Value;

/**
 * Business model to handle faster register operation
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableFastRegistry.class)
@JsonSerialize(as = ImmutableFastRegistry.class)
public interface FastRegistry extends Command {
	@Email
	String getEmail();
}
