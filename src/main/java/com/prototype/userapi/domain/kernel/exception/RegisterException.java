package com.prototype.userapi.domain.kernel.exception;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class RegisterException extends RuntimeException {

	public RegisterException(String message) {

		super(message);
	}
}
