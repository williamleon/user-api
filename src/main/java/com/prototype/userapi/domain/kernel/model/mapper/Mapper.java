package com.prototype.userapi.domain.kernel.model.mapper;

import com.prototype.userapi.domain.kernel.command.authentication.FastRegistry;
import com.prototype.userapi.domain.kernel.command.authentication.Registry;
import com.prototype.userapi.domain.kernel.model.Card;
import com.prototype.userapi.domain.kernel.model.ImmutableCard;
import com.prototype.userapi.domain.kernel.model.ImmutableUser;
import com.prototype.userapi.domain.kernel.model.ImmutableUserId;
import com.prototype.userapi.domain.kernel.model.ResultTransaction;
import com.prototype.userapi.domain.kernel.model.User;
import com.prototype.userapi.domain.kernel.model.UserId;
import com.prototype.userapi.domain.usecase.util.EncoderUtil;
import lombok.NoArgsConstructor;

/**
 * Mapper util for business objects on business layer
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@NoArgsConstructor
public class Mapper {

	public static final User userFormRegistry(Registry registry, EncoderUtil encoderUtil) {

		return ImmutableUser.builder()
							.email(registry.getEmail())
							.password(encoderUtil.encode(registry.getPassword()))
							.admin(registry.isAdmin()
										   .orElse(Boolean.FALSE))
							.build();
	}

	public static final User userFormFastRegistry(FastRegistry fastRegistry) {

		return ImmutableUser.builder()
							.email(fastRegistry.getEmail())
							.build();
	}

	public static final UserId userIdFormUser(User user) {

		return ImmutableUserId.builder()
							  .id(user.getId().get())
							  .build();
	}

	public static final Card fromResultTransaction(ResultTransaction result) {
		return ImmutableCard.builder()
							.creditCardTokenId(result.getCreditCardTokenId())
							.maskedNumber(result.getMaskedNumber())
							.name(result.getName())
							.paymentMethod(result.getPaymentMethod())
							.userId(result.getPayerId())
							.build();
	}

}
