package com.prototype.userapi.domain.kernel.model;

import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@Value.Style(get = {"is*", "get*"})
public interface Card {

	String getCreditCardTokenId();

	String getName();

	String getPaymentMethod();

	String getMaskedNumber();

	String getUserId();

}
