package com.prototype.userapi.domain.kernel.event.user;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.userapi.domain.kernel.event.Event;
import com.prototype.userapi.domain.kernel.model.User;
import org.immutables.value.Value;

/**
 * Interface to identify user events
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableUserEvent.class)
@JsonSerialize(as = ImmutableUserEvent.class)
public interface UserEvent extends Event {

	User getUser();
}
