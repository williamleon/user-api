package com.prototype.userapi.domain.kernel.command.authentication;

import javax.validation.constraints.Email;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.userapi.domain.kernel.command.Command;
import org.immutables.value.Value;

/**
 * Business model to handle login operation
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableLogin.class)
@JsonSerialize(as = ImmutableLogin.class)
public interface Login extends Command {

	@Email
	String getEmail();
	String getPassword();
}
