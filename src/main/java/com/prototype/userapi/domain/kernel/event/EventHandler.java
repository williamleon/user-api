package com.prototype.userapi.domain.kernel.event;

import com.prototype.userapi.domain.kernel.event.Event;

/**
 * Interface to define how to handler events {@link Event}
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface EventHandler <T extends Event> {

	void handle(T event);
}
