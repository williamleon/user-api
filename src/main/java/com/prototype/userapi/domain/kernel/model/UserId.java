package com.prototype.userapi.domain.kernel.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * Business object to handle user id
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableUserId.class)
@JsonSerialize(as = ImmutableUserId.class)
public interface UserId {

	String getId();
}
