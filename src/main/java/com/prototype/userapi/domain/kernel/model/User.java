package com.prototype.userapi.domain.kernel.model;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.immutables.value.Value;

/**
 * User model representation for business logic layer
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@Value.Style(get = {"is*", "get*"})
public interface User {

	Optional<String> getId();

	Optional<String> getName();

	Optional<String> getLastname();

	String getEmail();

	Optional<String> getIdentification();

	Optional<String> getPhoneNumber();

	Optional<String> getGender();

	@Value.Default
	default Boolean isAdmin() {
		return false;
	}

	@JsonIgnore
	Optional<String> getPassword();
}
