package com.prototype.userapi.domain.kernel.command.authentication;

import java.util.Optional;

import javax.validation.constraints.Email;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.userapi.domain.kernel.command.Command;
import org.immutables.value.Value;

/**
 * Business model to handle register operation
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@Value.Style(get = {"is*", "get*"})
@JsonDeserialize(as = ImmutableRegistry.class)
@JsonSerialize(as = ImmutableRegistry.class)
public interface Registry extends Command {

	@Email
	String getEmail();
	String getPassword();
	Optional<Boolean> isAdmin();
}
