package com.prototype.userapi.domain.kernel.exception;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public class TransactionException extends RuntimeException {

	public TransactionException(String message) {

		super(message);
	}
}
