package com.prototype.userapi.domain.kernel.command.user;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.prototype.userapi.domain.kernel.command.Command;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Immutable
@JsonDeserialize(as = ImmutableTokenizeCardCommand.class)
@JsonSerialize(as = ImmutableTokenizeCardCommand.class)
public interface TokenizeCardCommand extends Command {

	String getName();

	String getIdentificationNumber();

	String getPaymentMethod();

	String getNumber();

	String getExpirationDate();

	String getUserId();

}
