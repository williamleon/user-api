package com.prototype.userapi.domain.kernel.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Value.Style(get = {"is*", "get*"})
@Value.Immutable
@JsonDeserialize(as = ImmutableResultTransaction.class)
@JsonSerialize(as = ImmutableResultTransaction.class)
public interface ResultTransaction {

	String getCreditCardTokenId();

	String getName();

	String getIdentificationNumber();

	String getPaymentMethod();

	String getMaskedNumber();

	String getPayerId();

}
