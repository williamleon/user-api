package com.prototype.userapi.domain.usecase.service;

import com.prototype.userapi.domain.kernel.command.CommandExecute;
import com.prototype.userapi.domain.kernel.command.user.TokenizeCardCommand;
import com.prototype.userapi.domain.kernel.model.Card;
import com.prototype.userapi.domain.kernel.model.mapper.Mapper;
import com.prototype.userapi.domain.usecase.repository.CardRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class TokenizeCardCommandExecute implements CommandExecute<TokenizeCardCommand, Either<Throwable, Card>> {

	private final TransactionService transactionService;
	private final CardRepository cardRepository;

	/**
	 * Abstraction to define how to handle
	 *
	 * @param command
	 * @return
	 */
	@Override public Either<Throwable, Card> execute(final TokenizeCardCommand command) {

		log.info(String.format("Starting tokenized card process for [%s] and user [%s]", command.getNumber().substring(12),
							   command.getUserId()));
		return Try.of(() -> command)
				  .transform(commands -> Try.of(() -> transactionService.tokenizeCard(commands.get())))
				  .map(Mapper::fromResultTransaction)
				  .transform(cards -> Try.of(() -> cardRepository.save(cards.get())))
				  .peek(card -> log.info(String.format("Card tokenized [%s] for user [%s]", card.getMaskedNumber(), card.getUserId())))
				  .onFailure(throwable -> log
						  .error(String.format("Getting error tokenize card [%s] for user [%s]", command.getNumber().substring(12),
											   command.getUserId()), throwable))
				  .toEither();
	}
}
