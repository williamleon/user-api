package com.prototype.userapi.domain.usecase.service;

import com.prototype.userapi.domain.kernel.event.user.ImmutableUserEvent;
import com.prototype.userapi.domain.kernel.model.User;
import com.prototype.userapi.domain.kernel.query.QueryHandler;
import com.prototype.userapi.domain.kernel.query.user.UserInformationQuery;
import com.prototype.userapi.domain.usecase.repository.UserRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UserInformationQueryHandler implements QueryHandler<UserInformationQuery, Either<Throwable, User>> {

	private final UserRepository repository;

	/**
	 * Abstraction to define how to handle
	 *
	 * @param query
	 * @return
	 */
	@Override public Either<Throwable, User> handle(final UserInformationQuery query) {

		log.info(String.format("Starting query for user information [%s]...", query.getUserId()));
		return Try.of(() -> repository.findUserByEmail(query.getUserId()).get())
				  .peek(user -> log.info(String.format("Found user information [%s]", query.getUserId())))
				  .onFailure(throwable -> log.error("Getting error registering user", throwable))
				  .toEither();
	}
}
