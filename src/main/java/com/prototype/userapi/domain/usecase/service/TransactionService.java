package com.prototype.userapi.domain.usecase.service;

import com.prototype.userapi.domain.kernel.command.user.TokenizeCardCommand;
import com.prototype.userapi.domain.kernel.model.ResultTransaction;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface TransactionService {

	ResultTransaction tokenizeCard(TokenizeCardCommand command);
}
