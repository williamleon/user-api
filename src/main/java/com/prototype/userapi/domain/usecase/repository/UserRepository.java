package com.prototype.userapi.domain.usecase.repository;

import java.util.Optional;

import com.prototype.userapi.domain.kernel.model.User;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface UserRepository {

	Optional<User> findUserByEmail(String email);

	User save(User user);

	Optional<User> findById(String id);
}
