package com.prototype.userapi.domain.usecase.service;

import com.prototype.userapi.domain.kernel.command.CommandExecute;
import com.prototype.userapi.domain.kernel.command.authentication.CompleteRegistry;
import com.prototype.userapi.domain.kernel.exception.RegisterException;
import com.prototype.userapi.domain.kernel.model.ImmutableUser;
import com.prototype.userapi.domain.kernel.model.User;
import com.prototype.userapi.domain.usecase.repository.UserRepository;
import com.prototype.userapi.domain.usecase.util.EncoderUtil;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * CommandExecute implementation to execute Complete registry command
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class CompleteRegistryCommandExecute implements CommandExecute<CompleteRegistry, Either<Throwable, User>> {

	private static final String MESSAGE = "Getting error completing register for given mail %s";
	private final UserRepository userRepository;
	private final EncoderUtil encoderUtil;

	/**
	 * If given email is already in system, also evaluate if this user has password, and then if it has not password, update de record. To
	 * control the possible side effects is used {@link Either} structure, in case of a success process the {@link User} updated is returned
	 * in the right side of {@link Either}
	 *
	 * @param command complete registry command
	 * @return {@link Either} structure, handling correct response in right side and any possible throwable in left side
	 */
	@Override public Either<Throwable, User> execute(final CompleteRegistry command) {

		log.info("Starting complete registry command...");
		return Try.of(() -> userRepository.findUserByEmail(command.getEmail())
										  .filter(user -> !user.getPassword().isPresent())
										  .get())
				  .map(user -> (User) ImmutableUser.copyOf(user)
												   .withPassword(encoderUtil.encode(command.getPassword())))
				  .peek(user -> userRepository.save(user))
				  .onFailure(throwable -> log.error(String.format(MESSAGE, command.getEmail()), throwable))
				  .toEither(() -> new RegisterException(String.format(MESSAGE, command.getEmail())));
	}
}
