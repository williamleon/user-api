package com.prototype.userapi.domain.usecase.service;

import static com.prototype.userapi.domain.kernel.model.mapper.Mapper.userFormFastRegistry;

import com.prototype.userapi.domain.kernel.command.CommandExecute;
import com.prototype.userapi.domain.kernel.event.EventBus;
import com.prototype.userapi.domain.kernel.event.user.ImmutableUserEvent;
import com.prototype.userapi.domain.kernel.exception.RegisterException;
import com.prototype.userapi.domain.kernel.command.authentication.FastRegistry;
import com.prototype.userapi.domain.kernel.model.mapper.Mapper;
import com.prototype.userapi.domain.kernel.model.UserId;
import com.prototype.userapi.domain.usecase.repository.UserRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * CommandExecute implementation to execute fast registry command
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class FastRegistryCommandExecute implements CommandExecute<FastRegistry, Either<Throwable, UserId>> {

	private static final String MESSAGE = "Getting error doing faster register for given mail %s";
	private final UserRepository userRepository;
	private final EventBus eventBus;

	/**
	 * If given email is already in system, map the user information to {@link UserId} structure. In the other case, the email is saved,
	 * registering a new user, then the new user information is mapped to {@link UserId} too.
	 * To control the possible side effects is used {@link Either} structure, in case of a success process the {@link UserId} loaded/created
	 * is returned in the right side of {@link Either}
	 *
	 * @param command fast registry command
	 * @return {@link Either} structure, handling correct response in right side and any possible throwable in left side
	 */
	@Override public Either<Throwable, UserId> execute(final FastRegistry command) {

		log.info("Starting faster registry command...");
		return Try.of(() -> userRepository.findUserByEmail(command.getEmail())
										  .orElseGet(() -> userRepository.save(userFormFastRegistry(command))))
				  .peek(user -> log.info(String.format("User %s created or loaded successfully", user.getId())))
				  .peek(user -> eventBus.emit(ImmutableUserEvent.builder().user(user).build()))
				  .map(Mapper::userIdFormUser)
				  .onFailure(throwable -> log.error(String.format(MESSAGE, command.getEmail()), throwable))
				  .toEither(() -> new RegisterException(String.format(MESSAGE, command.getEmail())));
	}

}
