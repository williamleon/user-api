package com.prototype.userapi.domain.usecase.service;

import java.util.List;

import com.prototype.userapi.domain.kernel.model.Card;
import com.prototype.userapi.domain.kernel.query.QueryHandler;
import com.prototype.userapi.domain.kernel.query.user.FindCardsQuery;
import com.prototype.userapi.domain.usecase.repository.CardRepository;
import com.prototype.userapi.domain.usecase.repository.UserRepository;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class FindCardsQueryHandler implements QueryHandler<FindCardsQuery, Either<Throwable, List<Card>>> {

	private final CardRepository cardRepository;
	private final UserRepository userRepository;

	/**
	 * Abstraction to define how to handle
	 *
	 * @param query
	 * @return
	 */
	@Override public Either<Throwable, List<Card>> handle(final FindCardsQuery query) {

		log.info(String.format("Starting find tokenized cards query for user [%s]..", query.getUserId()));
		return Try.of(() -> userRepository.findUserByEmail(query.getUserId()).get())
				  .transform(users -> Try.of(() -> cardRepository.findAllByUserId(users.get().getId().get())))
				  .peek(cards -> log.info(String.format("Find [%s] tokenized cards for user [%s]", cards.size(), query.getUserId())))
				  .onFailure(throwable -> log
						  .error(String.format("Getting error find tokenized cards for user [%s]", query.getUserId()), throwable))
				  .toEither();
	}
}
