package com.prototype.userapi.domain.usecase.service;

import com.prototype.userapi.domain.kernel.event.EventHandler;
import com.prototype.userapi.domain.kernel.event.user.UserEvent;
import com.prototype.userapi.domain.usecase.stream.UserBinding;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UserEventHandler implements EventHandler<UserEvent> {

	private final UserBinding userBinding;

	@Override public void handle(final UserEvent event) {

		log.info(String.format("handling UserEvent for %s", event.getUser().getId()));
		userBinding.send(event);
	}
}
