package com.prototype.userapi.domain.usecase.repository;

import java.util.List;

import com.prototype.userapi.domain.kernel.model.Card;

/**
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface CardRepository {

	List<Card> findAllByUserId(String userId);

	Card save(Card card);
}
