package com.prototype.userapi.domain.usecase.stream;

import com.prototype.userapi.domain.kernel.event.user.UserEvent;

/**
 * Interface to binding user event with adapter
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface UserBinding {

	void send(UserEvent user);

}
