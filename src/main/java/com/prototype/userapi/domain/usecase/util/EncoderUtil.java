package com.prototype.userapi.domain.usecase.util;

/**
 * Abstract definition
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface EncoderUtil {

	/**
	 * encode definiton
	 *
	 * @param value
	 * @return
	 */
	String encode(String value);
}
