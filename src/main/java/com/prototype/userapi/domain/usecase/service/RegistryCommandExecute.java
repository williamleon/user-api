package com.prototype.userapi.domain.usecase.service;

import static com.prototype.userapi.domain.kernel.model.mapper.Mapper.userFormRegistry;

import com.prototype.userapi.domain.kernel.command.CommandExecute;
import com.prototype.userapi.domain.kernel.event.EventBus;
import com.prototype.userapi.domain.kernel.event.user.ImmutableUserEvent;
import com.prototype.userapi.domain.kernel.exception.RegisterException;
import com.prototype.userapi.domain.kernel.command.authentication.Registry;
import com.prototype.userapi.domain.kernel.model.User;
import com.prototype.userapi.domain.usecase.repository.UserRepository;
import com.prototype.userapi.domain.usecase.util.EncoderUtil;
import io.vavr.control.Either;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * CommandExecute implementation to execute Registry command
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RegistryCommandExecute implements CommandExecute<Registry, Either<Throwable, User>> {

	private final UserRepository userRepository;
	private final EncoderUtil encoderUtil;
	private final EventBus eventBus;

	/**
	 * Verify if given email is used for another user in the system, in negative case, create a record in the system. To control the
	 * possible side effects is used {@link Either} structure, in case of a success process the user created is returned in the right side
	 * of {@link Either} @param command
	 *
	 * @param command registry command
	 * @return {@link Either} structure, handling correct response in right side and any possible throwable in left side
	 */
	@Override public Either<Throwable, User> execute(final Registry command) {

		log.info("Starting registry command...");
		return Try.of(() -> verifyRegistryInformation(command))
				  .map(registry -> userRepository.save(userFormRegistry(registry, encoderUtil)))
				  .peek(user -> log.info(String.format("User %s created successfully", user.getId())))
				  .peek(user -> eventBus.emit(ImmutableUserEvent.builder().user(user).build()))
				  .onFailure(throwable -> log.error("Getting error registering user", throwable))
				  .toEither();
	}

	/**
	 * Validate if given email is already in system, in negative case, create a record in the system. To control the possible side effects
	 * is used {@link Either} structure, in case of a success process the user created is returned in the right side of {@link Either}
	 *
	 * @param registry registry command
	 * @return registry command
	 */
	private Registry verifyRegistryInformation(Registry registry) {

		if (userRepository.findUserByEmail(registry.getEmail()).isPresent()) {
			throw new RegisterException("Email already used");
		}
		return registry;
	}

}
