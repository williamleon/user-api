package com.prototype.userapi.domain.usecase.service;

import com.prototype.userapi.domain.kernel.command.CommandExecute;
import com.prototype.userapi.domain.kernel.command.authentication.Login;
import com.prototype.userapi.domain.kernel.model.TokenId;
import io.vavr.control.Either;

/**
 * CommandExecute extends to define how the login command will be implemented
 *
 * @author <a href="mailto:william.leon@payu.com">William Leon</a>
 * @since 0.0.1
 */
public interface ILoginCommandExecute extends CommandExecute<Login, Either<Throwable, TokenId>> {

}
